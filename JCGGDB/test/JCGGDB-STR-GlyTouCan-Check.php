<?php
header('Content-Type: text/plain;charset=UTF-8');
ini_set('auto_detect_line_endings', 1);
ini_set('auto_detect_line_endings', 1);
ini_set('error_reporting', E_ALL);
date_default_timezone_set('Asia/Tokyo');
$timeHeader = date("Y-m-d_H-i-s");
//$timeHeader = date("Y-m-d_");

/*
$directory_path = "results";
if(file_exists($directory_path)){
    echo "作成しようとしたディレクトリは既に存在します\n";
}else{
    if(mkdir($directory_path, 0777)){
        chmod($directory_path, 0777);
        echo "作成に成功しました\n";
    }else{
        echo "作成に失敗しました\n";
    }
}
*/

$file = "";
if (count($argv) > 0){
  $file = $argv[1];
}

$gtcfile = "";
if (count($argv) > 0){
  $gtcfile = $argv[2];
}


//$savepath = $directory_path.DIRECTORY_SEPARATOR.$file."_".$timeHeader.".txt";
//$errsavepath = $directory_path.DIRECTORY_SEPARATOR.$file."_".$timeHeader.".log.txt";


$DataString = "";

if(is_file($file)) {
    $lines = array();
    try {
        $filedata = file_get_contents($file);
        $str = str_replace(array("\r\n","\r","\n"), "\n", $filedata);
        $lines = explode("\n", $str);
    }
    catch (Exception $e) {
        //echo $e;
    }
    $count = 0;
    foreach ($lines as $line) {
        $count++;
        try {
            $url = "https://ts.glytoucan.org/sparql?query=PREFIX+foaf%3A+%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0D%0APREFIX+glycan%3A++%3Chttp%3A%2F%2Fpurl.jp%2Fbio%2F12%2Fglyco%2Fglycan%23%3E%0D%0APREFIX+glytoucan%3A++%3Chttp%3A%2F%2Fwww.glytoucan.org%2Fglyco%2Fowl%2Fglytoucan%23%3E%0D%0ASELECT+DISTINCT+%3FAccessionNumber%0D%0AFROM+%3Chttp%3A%2F%2Frdf.glytoucan.org%2Fcore%3E%0D%0AWHERE%7B%0D%0A++%3Fglycan+glytoucan%3Ahas_primary_id+%3FAccessionNumber+.%0D%0A++VALUES+%3FAccessionNumber+%7B+%22".$line."%22+%7D%0D%0A%7D&render=TSV&limit=25&offset=0#loadstar-results-section";
            $xml = file_get_contents($url);
            $gtc = new SimpleXMLElement($xml);
            
            if (!empty($gtc->results->result)) {
                $id = $gtc->results->result->binding->literal;
                if (strlen($id)>0){
                    echo $line, "\tGlyTouCan\t", $id, PHP_EOL;
                    $DataString .= $line."\tGlyTouCan\t".$id.PHP_EOL;
                    //file_put_contents($savepath, $DataString, FILE_APPEND | LOCK_EX); //追記モード
                }
                continue;
            }
            else {
                echo $line, "\tGlyTouCan\t", PHP_EOL;
                $DataString .= $line."\tGlyTouCan".PHP_EOL;
                //file_put_contents($savepath, $DataString, FILE_APPEND | LOCK_EX); //追記モード
                continue;
            }
        }
        catch (Exception $e) {
            //echo "ERROR: ".$line."/n";
            error_log($e);
        }
        
    }
}
//echo "wrote $file...\n".htmlspecialchars($file)."\n";

//echo "fin...";
?>